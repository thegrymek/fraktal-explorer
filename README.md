## [Fractal Explorer](http://simple.wikipedia.org/wiki/Fractal)

Fractal Explorer is another webpage for generating and exploring primary fractals f.e Mandelbrot, Julia or Plasma. 

Project uses:
    
* [HTML5 Boilerplate](http://html5boilerplate.com)
* CSS3 
* Web Workers
* jQuery

Example how it works you can find [here](http://mandelbrot.thegryme.vdl.pl/).

## Features

* Increase _Loops_ parameter to get better quality of rendered fractal. 
* Click on gradient to add/remove colors.
* If you do not want to zoom fractal and just want refresh page with the new parameters click button _Refresh_.
* _Custom_ fractal equation still doesn't work.
* _Workers_ number is a number of CPU threads used to calculating and drawing.  
* Reload page to get initial state.

## Quick start

Clone the repo  `git clone https://bitbucket.org/thegrymek/fraktal-explorer.git` - and run file _index.html_.


## Contributing

Anyone and everyone is welcome to contribute.

## License

Fractal Explorer is licensed as Free for Usage & Distribute.

