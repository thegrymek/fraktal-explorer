COMPUTING_METHODS = {
    ROWS : 'ROWS',
    COLUMNS : 'COLUMNS'
}
function Fractal(width, height, context){   
    this.maxLoop = 40;
    this.maxPointValue = 2.0;
    this.points = [];
    
    this.computeMethod = COMPUTING_METHODS.COLUMNS;
    this.storeMethod = this.computeMethod;
    this.width = width;
    this.height = height;
    if (typeof jQuery != 'undefined') {  
        this.getValuesFromPanel();
    }
};

/////////////////////////////////////////////////////////////////////
// Events
Fractal.prototype.init = function(params){};

Fractal.prototype.destroy = function(params){};

Fractal.prototype.onCompute = function(params){};

Fractal.prototype.onDraw = function(params) {};

/////////////////////////////////////////////////////////////////////
// Canvas Features
Fractal.prototype.zoom = function(percent){};

Fractal.prototype.zoomInFromRange = function(p1, p2){};

Fractal.prototype.zoomOut = function(){};

Fractal.prototype.zoomIn = function(percent) {};    
    
Fractal.prototype.getZoom = function() {
    return 1.0;
};

/////////////////////////////////////////////////////////////////////
// Computing and Drawing

Fractal.prototype.compute = function(x,y) {};
Fractal.prototype.computeByWorkers = function(oEvent){};
Fractal.prototype.computeRow = function(row){};
Fractal.prototype.computeColumn = function(column){};
Fractal.prototype.computeLine = function(line){};

Fractal.prototype.computeComplex = function(cpoint) {};

Fractal.prototype.draw = function() {};
Fractal.prototype.drawByWorkers = function(keys){};
    
Fractal.prototype.drawFractal = function(fractalmethod){};
Fractal.prototype.drawRow = function(points, row) {};
Fractal.prototype.drawColumn = function(points, column) {};
Fractal.prototype.drawLine = function(points, line) {};

Fractal.prototype.getColor = function(value) {
    return [0,0,0,255];
};

Fractal.prototype.getMaxLoop = function(){
    return this.maxLoop;
};

Fractal.prototype.getComputedLength = function(){
    if ( this.computeMethod == COMPUTING_METHODS.ROWS ) {
        return this.height;
    };
    if ( this.computeMethod == COMPUTING_METHODS.COLUMNS ) {
        return this.width;
    };
};

Fractal.prototype.setComputeMethod = function(method) {
    this.computeMethod = method;
    this.storeMethod = this.computeMethod;
};

Fractal.prototype.updatePoints = function(line, points) {
    this.points[line] = points;
};

Fractal.prototype.resetPoints = function() {
    this.points = [];
};

Fractal.prototype.getPoints = function(line) {
    if ( line != undefined )
        return this.points[line];
    else
        return this.points;
};

Fractal.prototype.reconstruct = function(params) {
    if ( 'maxLoop' in params ) this.maxLoop = params['maxLoop'];
    if ( 'maxPointValue' in params ) this.maxPointValue = params['maxPointValue'];
    if ( 'computeMethod' in params ) this.setComputeMethod(params['computeMethod']);
};

Fractal.prototype.getValuesFromPanel = function() {
    this.keepRatio = $('#julia-coordinates-keepratio').val();
    this.maxLoop = $('#julia-performance-maxloop').val();
    this.maxPointValue = $('#julia-performance-maxvaluepoint').val();
};

Fractal.prototype.getStartPosition = function() {
    return new Complex(0.0, 0.0);
};

Fractal.prototype.getEndPosition = function() {
    return new Complex(0.0, 0.0);
};