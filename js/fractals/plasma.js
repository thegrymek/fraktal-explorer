
function Plasma(width, height){
    Fractal.call(this, width, height);
    
    this.length = Math.max(this.width, this.height);
    for(var i=0; i <= this.length+5; i++){
        this.points[i] = [];
    };
    
    this.red = 0;
    this.blue = 0;
    this.green = 0;
    
    this.p1 = Math.random();
    this.p2 = Math.random();
    this.p3 = Math.random();
    this.p4 = Math.random();
    
//     this.colorModif = [255, 255, 255];
}

extend(Fractal, Plasma, Fractal.prototype);

Plasma.prototype.init = function(params){
    Fractal.prototype.init.call(this, params);
    
};

Plasma.prototype.destroy = function(params){
    Fractal.prototype.destroy.call(this, params);
    
};

Plasma.prototype.draw = function(){
    this.resetPoints();
    this.resetCorners();
    this.splitPoints(this.getPoints(),
                       this.p1,
                       this.p2,
                       this.p3,
                       this.p4,
                       0, 0,
                       this.length, this.length);
    var points = this.points;
    var ctx = canvasEngine.context;
    var gradient = canvasEngine.getSelectedGradient();    
    var x = 0;
    function drawPlasma(){
        var c = ctx.createImageData(1, points.length);
        var cinc = 0;
        for(var y=0; y<points[x].length; y++) {
            var value = gradient.convertPercentToPosition(points[x][y]);
            var color = gradient.colorTable[value];
            c.data[cinc] = color[0];
            c.data[++cinc] = color[1];
            c.data[++cinc] = color[2];
            c.data[++cinc] = 255;
            ++cinc;
        };
        ctx.putImageData(c, x, 0);
        x++;
        if(x < points.length) setTimeout(drawPlasma, 0);
    }
    drawPlasma();
    
};

Plasma.prototype.resetPoints = function() {
    this.points = [];
    this.length = Math.max(this.width, this.height);
    for(var i=0; i <= this.length+5; i++){
        this.points[i] = [];
    };
};


Plasma.prototype.resetCorners = function() {
    this.p1 = Math.random();
    this.p2 = Math.random();
    this.p3 = Math.random();
    this.p4 = Math.random();
};

Plasma.prototype.updateCorners = function(p1, p2, p3, p4){
    this.p1 = p1;
    this.p2 = p2;
    this.p3 = p3;
    this.p4 = p4;
};

Plasma.prototype.normalize = function(val){
    return (val < 0) ? 0 : (val > 1) ? 1 : val;
};

Plasma.prototype.splitPoints = function(points, p1, p2, p3, p4, x, y, width, height) {        
    if ( width > 1 || height > 1 ){
        var side1, side2, side3, side4, center;
        var transWidth = parseInt(width / 2);
        var transHeight = parseInt(height / 2);
        
        var center = this.normalize( (p1 + p2 + p3 + p4)/4 );
        center += this.shift(transWidth+transHeight);
        
        var side1 = this.normalize( (p1+p2) / 2 );
        var side2 = this.normalize( (p2+p3) / 2 );
        var side3 = this.normalize( (p3+p4) / 2 );
        var side4 = this.normalize( (p4+p1) / 2 );
        
        this.splitPoints(points, p1, side1, center, side4,  x, y, transWidth, transHeight);
        this.splitPoints(points, side1, p2, side2, center,  x+transWidth, y, width-transWidth, transHeight);
        this.splitPoints(points, center, side2, p3, side3,  x+transWidth, y+transHeight, width-transWidth, height-transHeight);
        this.splitPoints(points, side4, center, side3, p4,  x, y+transHeight, transWidth, height-transHeight);
    }else{
        points[x][y] = (p1 + p2 + p3 + p4)/4;
    };
};

Plasma.prototype.computePointsByWorkers = function(workersNumber) {
    
};

Plasma.prototype.shift = function(smallSize) {
    return (Math.random() - 0.5) * smallSize / this.length*2 * 2;
}

Plasma.prototype.getColor = function(value){
    var gradient = canvasEngine.getSelectedGradient();
    var position = gradient.convertPercentToPosition(value);
    return gradient.getColor(position);
}

Plasma.prototype.zoomInFromRange = function(p1, p2) {
    this.p1 = this.points[p1.x][p1.y];
    this.p2 = this.points[p2.x][p1.y];
    this.p3 = this.points[p2.x][p2.y];
    this.p4 = this.points[p1.x][p2.y];
};

Plasma.prototype.zoom = function(percent){
    percent = percent || 1.25;
    if ( percent < 1 ){
        var transWidth = parseInt(this.width * (1-percent));
        var transHeight = parseInt(this.height * (1-percent));
        this.p1 = this.points[transWidth][transHeight];
        this.p2 = this.points[this.width-transWidth][transHeight];
        this.p3 = this.points[this.width-transWidth][this.height-transHeight];
        this.p4 = this.points[transWidth][this.height-transHeight];
    }else
    {
        var transWidth = parseInt(this.width * (percent-1));
        var transHeight = parseInt(this.height * (percent-1));
        
        var center = this.getCenter();
        var vp1 = this.points[transWidth][transHeight];
        var vp2 = this.points[this.width-transWidth][transHeight];
        var vp3 = this.points[this.width-transWidth][this.height-transHeight];
        var vp4 = this.points[transWidth][this.height-transHeight];
        
        this.p1 = this.p1 - center > 0 ? this.p1 + vp1 : this.p1 - vp1;
        this.p2 = this.p2 - center > 0 ? this.p2 + vp2 : this.p2 - vp2;
        this.p3 = this.p3 - center > 0 ? this.p3 + vp3 : this.p3 - vp3;
        this.p4 = this.p4 - center > 0 ? this.p4 + vp4 : this.p4 - vp4;
        
        this.p1 = this.normalize(this.p1);
        this.p2 = this.normalize(this.p2);
        this.p3 = this.normalize(this.p3);
        this.p4 = this.normalize(this.p4);
    }
};

Plasma.prototype.zoomIn = function(){
    this.zoom(0.90);
};

Plasma.prototype.zoomOut = function(){
    this.zoom(1.25);
};

Plasma.prototype.getCenter = function(p1,p2,p3,p4){
    return (p1+p2+p3+p4)/4;
};

Plasma.prototype.convertToComplex = function(c1, c2) {
    return new Complex(0,0);
};