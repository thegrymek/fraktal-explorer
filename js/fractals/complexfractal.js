ComplexFractal = function(width, height, context){
    Fractal.call(this, width, height, context);
    
    this.keepRatio = true;
    this.startComplexPos = new Complex(-2.5, -1.1);
    this.updateScales(this.startComplexPos, this.startComplexPos.inverse());

};

extend(Fractal, ComplexFractal, Fractal.prototype);

ComplexFractal.prototype.reconstruct = function(params) {
    Fractal.prototype.reconstruct.call(this, params);
    if ( 'startA' in params && 'startJ' in params ){
        this.startComplexPos = new Complex( params['startA'], params['startJ'] );
    }
    if ( 'scaleAX' in params ) this.scaleAX = params['scaleAX'];
    if ( 'scaleIY' in params ) this.scaleIY = params['scaleIY'];
    if ( 'keepRatio' in params ) this.keepRatio = params['keepRatio'];
};

/////////////////////////////////////////////////////////////////////
// Events
ComplexFractal.prototype.onDraw = function(params){
    var result = Fractal.prototype.onDraw.call(this, params);
    if ( canvasEngine ) {
        canvasEngine.drawnLines++;
        canvasEngine.updateProgressBars();
    };
    return result;
};

ComplexFractal.prototype.onCompute = function(params) {
    var result = Fractal.prototype.onCompute.call(this, params);
    if ( canvasEngine ) {
        canvasEngine.computedLines++;
        canvasEngine.updateProgressBars();
    }
    return result;
};

ComplexFractal.prototype.init = function(params) {
    Fractal.prototype.init.call(this, params);
    $('#control-panel-fractal-performance').show();
    $('#julia-fractal-equation').show();
    $('#control-panel-coordinates').show();
};


ComplexFractal.prototype.destroy = function(params) {
    Fractal.prototype.destroy.call(this, params);
    $('#control-panel-fractal-performance').hide();
    $('#julia-fractal-equation').hide();
    $('#control-panel-coordinates').hide();
};

/////////////////////////////////////////////////////////////////////
// Drawing and Computing

ComplexFractal.prototype.getColor = function(value){
    var gradient = canvasEngine.getSelectedGradient();
    var position = gradient.convertPercentToPosition(value/this.maxLoop);
    var color = gradient.colorTable[position];
    return color;
};

ComplexFractal.prototype.computeRow = function(row){
    result = [];
    var sizeJ = this.getSizeJ();
    var sizeA = this.getSizeA();
    var cpoint = this.startComplexPos.copy();
    cpoint.j += row*sizeJ;
    for (var x = 0; x < this.width; x++, cpoint.a+=sizeA) {       
        var n = this.computeComplex(cpoint);
        result.push(n);
    }
    return result;
}

ComplexFractal.prototype.computeColumn = function(column){
    result = [];
    var sizeJ = this.getSizeJ();
    var sizeA = this.getSizeA();
    var cpoint = this.startComplexPos.copy();
    cpoint.a += column*sizeA;
    for (var y = 0; y < this.height; y++, cpoint.j+=sizeJ) {            
        var n = this.computeComplex(cpoint);
        result.push(n);
    }
    return result;
}

ComplexFractal.prototype.computeLine = function(line){
    this.onCompute({'computeLine': line, 'canvasEngine': canvasEngine});
    if ( this.computeMethod == COMPUTING_METHODS.COLUMNS ) {
        return this.computeColumn(line);
    }
    if ( this.computeMethod == COMPUTING_METHODS.ROWS ) {
        return this.computeRow(line);
    }
};

ComplexFractal.prototype.drawRow = function(row){    
    var ctx = canvasEngine.context;
    var gradient = canvasEngine.getSelectedGradient();
    var fractal = canvasEngine.fractal;
    var maxLoop = fractal.maxLoop;
    var points = fractal.getPoints(row);
    
    var c = ctx.createImageData(1, points.length);
    var y = 0;
    var cinc = 0;
    for(var y=0; y<points.length; y++) {
        var value = gradient.convertPercentToPosition(points[y]/maxLoop);
        var color = gradient.colorTable[value];
        c.data[cinc] = color[0];
        c.data[++cinc] = color[1];
        c.data[++cinc] = color[2];
        c.data[++cinc] = 256;
        ++cinc;
    };
    ctx.putImageData(c, 0, row);
};

ComplexFractal.prototype.drawColumn = function(column) {
    var ctx = canvasEngine.context;
    var gradient = canvasEngine.getSelectedGradient();
    var fractal = canvasEngine.fractal;
    var maxLoop = fractal.maxLoop;
    var points = fractal.getPoints(column);
    
    var c = ctx.createImageData(1, points.length);
    var y = 0;
    var cinc = 0;
    for(var y=0; y<points.length; y++) {
        var value = gradient.convertPercentToPosition(points[y]/maxLoop);
        var color = gradient.colorTable[value];
        c.data[cinc] = color[0];
        c.data[++cinc] = color[1];
        c.data[++cinc] = color[2];
        c.data[++cinc] = 256;
        ++cinc;
    };
    ctx.putImageData(c, column, 0);
    
};

ComplexFractal.prototype.drawLine = function(line){
    this.onDraw({'drawLine': line, 'canvasEngine': canvasEngine});
    if ( this.computeMethod == COMPUTING_METHODS.COLUMNS ) {
        return this.drawColumn(line);
    }
    if ( this.computeMethod == COMPUTING_METHODS.ROWS ) {
        return this.drawRow(line);
    }
};

ComplexFractal.prototype.computeByWorkers = function(oEvent){
    var data = oEvent.data;
    var fractal = canvasEngine.fractal;
//     if ( data['state'] == 'working'){
        fractal.updatePoints(data['line'], data['points']);
//     }
//     if ( data['state'] == 'done'){
        fractal.drawByWorkers([data['line']]);
//         return;
//     }
};

ComplexFractal.prototype.drawByWorkers = function(lines){
    var engine = canvasEngine;
    var data = engine.fractal.getPoints();
    var method = engine.fractal.storeMethod;
    var fractal = engine.fractal;
    
    lines = lines || Object.keys(data);
    if ( lines.length < 1 ) return;
    
    for(var i=0; i<lines.length; i++){
        var line = lines[i];
        fractal.drawLine(line);
    };
};

ComplexFractal.prototype.drawFractal = function(){
    var line = 0;
    var range = this.getComputedLength();
    var fractal = canvasEngine.fractal;
    this.resetPoints();
    for(line=0; line < range; line++){
        console.log(line);
        var points = this.computeLine(line);
        this.updatePoints(line, points);
        this.drawLine(line);
    }

//     line = 0;
//     function computeAndDraw()
//     {
//         var points = fractal.computeLine(line);
//         console.log(line);
//         fractal.updatePoints(line, points);
//         fractal.drawLine(line);
//         if ( line < range ){
//             line+=2;
//             setTimeout(computeAndDraw, 0);
//         }else if (line % 2 == 0){
//             line = 1;
//             setTimeout(computeAndDraw, 0);
//         }
//     };
//     computeAndDraw();
};


ComplexFractal.prototype.createMessage = function()
{
    return message = {
            'maxLoop': canvasEngine.maxLoop,
            'maxPointValue': canvasEngine.maxPointValue,
            'keepRatio': canvasEngine.keepRatio,
            'computeMethod': this.computeMethod,
            'width': this.width,
            'height': this.height,
            'startA': this.startComplexPos.a,
            'startJ': this.startComplexPos.j,
            'scaleAX': this.scaleAX,
            'scaleIY': this.scaleIY,
            'workers': canvasEngine.maxFractalWorkers,
    };
}

ComplexFractal.prototype.draw = function() {
    this.resetPoints();
    if (typeof(Worker)!=="undefined" && canvasEngine.maxFractalWorkers > 0)
    {
        var message = this.createMessage();
        this.resetPoints();
        console.log(message);
        for(var w = 0; w < canvasEngine.maxFractalWorkers; w++){
            var worker = new Worker('js/fractals/fractalworker.js');
            worker.onmessage = ComplexFractal.prototype.computeByWorkers;
            message['worker'] = w;
            worker.postMessage(message);
            canvasEngine.workers[w] = worker;
        }
    }
    else
    {
        this.drawFractal();
    }
};
/////////////////////////////////////////////////////////////////////
// Canvas Features

ComplexFractal.prototype.zoomInFromRange = function(p1, p2){
    // getting min and max points
    var minx = Math.min(p1.x,p2.x); var maxx = Math.max(p1.x,p2.x);
    var miny = Math.min(p1.y,p2.y); var maxy = Math.max(p2.y,p2.y);
    
    // converting to complex
    var c1 = this.convertToComplex(minx, miny);
    var c2 = this.convertToComplex(maxx, maxy);
    
    // updating start complex point
    this.startComplexPos = c1.copy();
    
    this.updateScales(c1, c2);
};

ComplexFractal.prototype.zoom = function(percent) {
    percent = percent || 1.25;
    var complexBorder = this.getComplexPlotRange();
    
    var complexBorderA = this.getComplexPlotARange();
    var complexBorderJ = this.getComplexPlotJRange();
    
    var lengthPlotA = Math.abs(complexBorder['a'][1]-complexBorder['a'][0]);
    var lengthPlotJ = Math.abs(complexBorder['j'][1]-complexBorder['j'][0]);
    
    var lenghtA = percent * lengthPlotA;
    var lengthJ = percent * lengthPlotJ;
    
    var diffA = lenghtA - lengthPlotA;
    var diffJ = lengthJ - lengthPlotJ;
    var absPoint = new Complex(diffA, diffJ);
    
    this.startComplexPos = this.startComplexPos.sub(absPoint);
    var endComplexPos = new Complex(complexBorder['a'][1],complexBorder['j'][1]);
    endComplexPos = endComplexPos.add(absPoint);
    
    this.updateScales(this.startComplexPos, endComplexPos);
};

ComplexFractal.prototype.zoomOut = function(){
    this.zoom(1.25);
};

ComplexFractal.prototype.zoomIn = function() {
    this.zoom(0.75);
};

ComplexFractal.prototype.getZoom = function() {
    var border = canvasEngine.maxPointValue;
    var startPos = new Complex(-border, -border);
    var endPos = new Complex(border, border);
    
    var scaleAX = this.getScaleFromComplexRange(startPos.a, endPos.a, this.width);
    var scaleIY = this.getScaleFromComplexRange(startPos.j, endPos.j, this.width);
    
    return {
        'a': scaleAX/this.scaleAX,
        'j': scaleIY/this.scaleIY,
    }
};

/////////////////////////////////////////////////////////////////////
// XY Methods

ComplexFractal.prototype.getMidPoint = function(){
    return new Point(this.width/2, this.height/2);
};

ComplexFractal.prototype.getDensityFromRange = function(x1,x2, width) {
    return Math.abs(x2-x1)/width;
};

ComplexFractal.prototype.getStartPosition = function() {
    return this.startComplexPos;
}

ComplexFractal.prototype.getEndPosition = function() {
    var range = this.getComplexPlotRange();
    return new Complex( range['a'][1], range['j'][1] );
}


/////////////////////////////////////////////////////////////////////
// Complex Methods

ComplexFractal.prototype.getScaleFromComplexRange = function(a1, a2, width) {
    return width/Math.abs(a2-a1);
};

// Function returns range of A axis
ComplexFractal.prototype.getComplexPlotARange = function(){
    return [this.startComplexPos.a, this.startComplexPos.a + (this.width / this.scaleAX)];
};

// Function returns range of J axis
ComplexFractal.prototype.getComplexPlotJRange = function(){
    return [this.startComplexPos.j, this.startComplexPos.j + (this.height / this.scaleIY)];
};

// Function compute range in complex plot
// returns dict of list x and y keys.
ComplexFractal.prototype.getComplexPlotRange = function(){
    return {
        'a' : this.getComplexPlotARange(),
        'j' : this.getComplexPlotJRange()
    };
};

ComplexFractal.prototype.convertComplexToXY = function(a,j){
    // TODO
};

// returns size of one pixel in complex
ComplexFractal.prototype.getSizeA = function(){
    var range = this.getComplexPlotARange();
    var absX = Math.abs(range[1] - range[0]);
    var a = absX / (this.width);
    return a;
};

// returns size of Y in complex
ComplexFractal.prototype.getSizeJ = function(){
    var range = this.getComplexPlotJRange();
    var absY = Math.abs(range[1] - range[0]);
    var j = absY / (this.height);
    return j;
};

// convert x position into a.complex 
ComplexFractal.prototype.convertXtoA = function(x){
    return this.getSizeA() * x + this.startComplexPos.a;
};

// convert y position into j complex
ComplexFractal.prototype.convertYtoJ = function(y) {
    return this.getSizeJ() * y + this.startComplexPos.j;
};

// function converts xy point into complex
ComplexFractal.prototype.convertToComplex = function(x,y){
    return new Complex(this.convertXtoA(x), this.convertYtoJ(y));
};

ComplexFractal.prototype.updateScales = function(c1, c2) {
    var newScaleAX = this.getScaleFromComplexRange(c1.a, c2.a, this.width);
    var newScaleIY = this.getScaleFromComplexRange(c1.j, c2.j, this.height);

    if (this.keepRatio == true){
        if ( newScaleAX > newScaleIY){
            newScaleIY = newScaleAX;
        }else{
            newScaleAX = newScaleIY;
        };
    };
    
    // updating scales
    this.scaleAX = newScaleAX;
    this.scaleIY = newScaleIY;
};