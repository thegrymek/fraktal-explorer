importScripts('../plugins.js');
importScripts('complex.js');
importScripts('fractal.js');
importScripts('complexfractal.js');
importScripts('mandelbrot.js');
importScripts('julia.js');

onmessage = function(oEvent){
    var data = oEvent.data;
    var width = data['width'];
    var height = data['height'];
    var type = data['fractal'];
    var fractal = false;
    if ( type == 'julia' ){
        fractal = new Julia(width, height);
    };
    if ( type == 'mandelbrot' ){
        fractal = new Mandelbrot(width, height);
    }
    fractal.reconstruct(data);

    var result = {
        points:[],
        line: 0,
        'lines': [],
        'state': 'working',
        'worker': data['worker'],
    };
    var range = fractal.getComputedLength();
    var amount = parseInt(range/data['workers']);
    for (var i = data['worker']; i < range; i+=data['workers']){
//     for (var i = amount*data['worker']; i < amount*(data['worker']+1); i++){
        result['line'] = i;
        result['lines'].push(i);
        result['points'] = fractal.computeLine(i);
        postMessage(result);
    }
    
    result['state'] = 'done';
    postMessage(result);
    self.close();
};