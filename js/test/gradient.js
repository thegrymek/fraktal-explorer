
GradientTest = TestCase("GradientTest");

GradientTest.prototype.setUp = function(){
    var canvas = document.getElementById("canvas-gradient");
    this.gradient = new Gradient(canvas);
};

GradientTest.prototype.testCanvas = function(){
    var canvas = document.getElementById("canvas-gradient");
    assertNotUndefined("Canvas is not null ", canvas);
};

GradientTest.prototype.testColor = function(){
    this.gradient.addColorStop(0, 'rgb(0,0,0)');
    this.gradient.addColorStop(0, 'rgb(255,255,255)');
    this.gradient.updateGradient();
    var color = this.gradient.getColor(0);
    assertString('Is not string', color);
    assertEquals('Not same color', '#000000', color);
};