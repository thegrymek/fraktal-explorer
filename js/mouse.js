
drawIfMoving = false;
startMousePositionX = 0;
startMousePositionY = 0;
rectWidth = 0;
rectHeight = 0;

/////////////////////////////////////////////////////////////////////////////
// Canvas Fractal Methods

function onMouseDown(e, obj) {
    e = e || window.event; // window.event for IE

    drawIfMoving = true;
    startMousePositionX = e.pageX;
    startMousePositionY = e.pageY;
};

function onMouseUp(e, obj) {
    e = e || window.event; // window.event for IE

    drawIfMoving = false;
    var canvasSelection = document.getElementById('canvas-selection');
    var contextSelection = canvasSelection.getContext('2d');
    
    if ( rectWidth < 4 || rectHeight < 4 ) {
            return;
    }
    
    // clearing whole canvas
    contextSelection.clearRect(0, 0, contextSelection.canvas.width, contextSelection.canvas.height);;
    
    var canvasFractal = document.getElementById('canvas-fractal'); 
    
    canvasEngine.clear();
    canvasEngine.resetProgressBars();
    
    canvasEngine.zoomInFromRange(new Point(startMousePositionX, startMousePositionY), 
                                new Point(startMousePositionX + rectWidth, startMousePositionY + rectHeight));
    canvasEngine.drawFractal();
    canvasEngine.updatePanel();
}

function onMouseMove(e, obj) {


    e = e || window.event;
    var canvas = document.getElementById('canvas-selection');
    var context = canvas.getContext('2d');

    context.clearRect(0, 0, canvas.width, canvas.height);

    if (drawIfMoving == true) {
        context.beginPath();
        if ( $('#julia-coordinates-keepratio').is(':checked') ) {
            rectWidth = e.pageX - startMousePositionX;
            rectHeight = rectWidth * h / w;
        }else
        {
            rectWidth = e.pageX - startMousePositionX;
            rectHeight = e.pageY - startMousePositionY;
        }
        
        context.rect(startMousePositionX, startMousePositionY, rectWidth, rectHeight);
        context.fillStyle = 'rgba(255, 255, 255, 0.05)';
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = '#444';
        context.stroke();
    }


    var complexPosition = canvasEngine.fractal.convertToComplex(e.pageX, e.pageY);

    $('#julia-position-a').html(complexPosition.a.toExponential(EXP_PRECISION).toString());
    $('#julia-position-j').html(complexPosition.j.toExponential(EXP_PRECISION).toString());
    // context.endPath();
};

///////////////////////////////////////////////////////////////////////////
// Canvas Gradient Methods
function getMouse(e, canvas) {
  var element = canvas, offsetX = 0, offsetY = 0, mx, my;
//   stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingLeft'], 10)      || 0;
//   stylePaddingTop  = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingTop'], 10)       || 0;
//   styleBorderLeft  = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderLeftWidth'], 10)  || 0;
//   styleBorderTop   = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderTopWidth'], 10)   || 0;
  // Some pages have fixed-position bars (like the stumbleupon bar) at the top or left of the page
  // They will mess up mouse coordinates and this fixes that
  var html = document.body.parentNode;
  htmlTop = html.offsetTop;
  htmlLeft = html.offsetLeft;
  // Compute the total offset. It's possible to cache this if you want
  if (element.offsetParent !== undefined) {
    do {
      offsetX += element.offsetLeft;
      offsetY += element.offsetTop;
    } while ((element = element.offsetParent));
  }

  // Add padding and border style widths to offset
  // Also add the <html> offsets in case there's a position:fixed bar (like the stumbleupon bar)
  // This part is not strictly necessary, it depends on your styling
//   offsetX += stylePaddingLeft + styleBorderLeft + htmlLeft;
//   offsetY += stylePaddingTop + styleBorderTop + htmlTop;

  mx = e.pageX - offsetX;
  my = e.pageY - offsetY;

  // We return a simple javascript object with x and y defined
  return {x: mx, y: my};
}

function onGradientClick(e, obj) {
    e = e || window.event;
    var canvas = document.getElementById("canvas-gradient");
    var mouse = getMouse(e, canvas);
    var stopRemoved = false;
    
    var cssWidth = e.target.clientWidth;
    var canvasWidth = e.target.width;
    
    var cssHeight = e.target.clientHeight;
    var canvasHeight = e.target.height;
    
    var posX = ~~(mouse['x'] * canvasWidth / cssWidth);
    var posY = ~~(mouse['y'] * canvasWidth / cssWidth);
    
    var gradient = canvasEngine.gradient;
    var stops = gradient.stops;
    var percent = ~~(gradient.width * 0.1);
    
    if ( Object.keys(stops).length < 2 ) return;
    for(var stop in stops){
        var position = gradient.convertPercentToPosition(stop);
        if ( Math.abs(position - posX) < percent) {
            delete gradient.stops[stop];
            stopRemoved = true;
            gradient.recompute();
            gradient.drawIntervals();
            break;
        }
    }
    
    if ( !stopRemoved ){
        var colorPicker = $("#canvas-color-picker");
        newGradientPosition = gradient.convertPositionToPercent(posX);
        console.log(newGradientPosition);
        colorPicker.show();
        
    }
    
};

function onColorPick(e, obj){
    e = e || window.event;
    var colorPicker = document.getElementById("canvas-color-picker");
    var mouse = getMouse(e, colorPicker);
    var data = colorPicker.getContext('2d').getImageData(mouse['x'], mouse['y'], 1,1).data;
    
    var color = canvasEngine.gradient.convertDataToHexColor(data);
    canvasEngine.gradient.addColorStop(newGradientPosition, 'rgb('+data[0]+','+data[1]+','+data[2]+')');
    canvasEngine.gradient.recompute();
    canvasEngine.gradient.drawIntervals();
    $("#canvas-color-picker").hide();
};