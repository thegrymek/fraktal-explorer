canvasEngine = false;

// Place any jQuery/helper plugins in here.

// http://stackoverflow.com/questions/4152931/javascript-inheritance-call-super-constructor-or-use-prototype-chain
// This is a constructor that is used to setup inheritance without
// invoking the base's constructor. It does nothing, so it doesn't
// create properties on the prototype like our previous example did
function surrogateCtor() {
}

function extend(base, sub, methods) {
    // Copy the prototype from the base to setup inheritance
    surrogateCtor.prototype = base.prototype;
    // Tricky huh?
    sub.prototype = new surrogateCtor();
    // Remember the constructor property was set wrong, let's fix it
    sub.prototype.constructor = sub;
    // Copy the methods passed in to the prototype
    for ( var name in methods) {

        if (!sub.prototype.hasOwnProperty(name)) {
            sub.prototype[name] = methods[name];
        }

    }
}

Array.prototype.pushArray = function(array){
    for(var i in array){
        this.push(array[i]);
    };
};
/*
Array.prototype.clear = function(){
    this = new Array();
};*/