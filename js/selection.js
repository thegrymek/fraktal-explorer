
drawIfMoving = false;
lastMousePositionX = 0;
lastMousePositionY = 0;

function onMouseDown(e, obj) {
	e = e || window.event; // window.event for IE

	drawIfMoving = true;
	lastMousePositionX = e.pageX;
	lastMousePositionY = e.pageY;
};

function onMouseUp(e, obj) {
    e = e || window.event; // window.event for IE

    drawIfMoving = false;
    var canvas = document.getElementById('canvas-selection');
    var context = canvas.getContext('2d');
    
    // clearing whole canvas
    context.clear();
    
    var complexCanvas = document.getElementById('canvas-fractal'); 
//     complexCanvas.getContext('2d').clear();
  
    // updating constans
    canvasEngine.zoomInFromRange(new Point(lastMousePositionX, lastMousePositionY), 
                                new Point(e.pageX, e.pageY));
    canvasEngine.drawFractal();
}

function onMouseMove(e, obj) {
    e = e || window.event;
    var canvas = document.getElementById('canvas-selection');
    var context = canvas.getContext('2d');
    var selection = $('#options-selection ');

    context.clear();

    if (drawIfMoving == true) {
        context.beginPath();
        context.rect(lastMousePositionX, lastMousePositionY, e.pageX - lastMousePositionX, e.pageY
                - lastMousePositionY);
        context.fillStyle = 'rgba(255, 255, 255, 0.05)';
        //console.log(selection.css('color'));
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = '#444';
        context.stroke();
    }

    context.font = "12pt Helvetica";

    var complexPosition = canvasEngine.convertToComplex(e.pageX, e.pageY);
    var offsetX = 20; var offsetY = 20;
    if (e.pageX > (95/100 * canvas.width)){
        offsetX *= -3;
    };
    if (e.pageY > 95/100 * canvas.height){
        offsetY *= -1;
    };
    context.strokeText('a: '+complexPosition.a.toString(), e.pageX+offsetX, e.pageY+offsetY);
    context.strokeText('i: '+complexPosition.j.toString(), e.pageX+offsetX, e.pageY+2*offsetY);

    // context.endPath();
};
